﻿using CATPLMEnvBuild;
using HybridShapeTypeLib;
using INFITF;
using KnowledgewareTypeLib;
using MECMOD;
using PARTITF;
using ProductStructureClientIDL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection.Metadata;
using System.Runtime.CompilerServices;
using System.Text;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace niet_3dx
{
    internal class niet3DxConnection
    {
        #region init
        private INFITF.Application hsp_experienceApp;
        private Editor hsp_experienceEditor;
        private Sketch hsp_catiaSkizze;

        private ShapeFactory SF;
        private HybridShapeFactory HSF;
        private Pad mySchaft;
        private Body myBody;
        private Part myPart;
        private Sketches mySketches;
        #endregion

        public bool CATIALaeuft()
        {
            try
            {  
                object experienceObject = Marshal2.GetActiveObject("CATIA.Application");
                hsp_experienceApp = (INFITF.Application)experienceObject;

                // Prüfen, ob auch wirklich 3DEXPERIENCE gefunden wurde.
                if (hsp_experienceApp.get_Name() != "3DEXPERIENCE")
                {
                    throw new Exception();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public void Erzeuge3DShape()
        {

            PLMNewService newService = (PLMNewService)hsp_experienceApp.GetSessionService("PLMNewService");
           
            // Ein "Editor"-Objekt wird als Outpout zurückgeliefert.
            newService.PLMCreate("3DShape", out hsp_experienceEditor);

            // Part-Objekt bekommen.
            myPart = (Part)hsp_experienceEditor.ActiveObject;

            // 3DShape bekommen.
            VPMRepReference myProductRepresentationReference = (VPMRepReference)myPart.Parent;

            
            myProductRepresentationReference.SetAttributeValue("PLM_ExternalID", "01");

   
            myProductRepresentationReference.SetAttributeValue("Name", "niet");


            myPart.Update();
        }


        public void ErstelleLeereSkizze()
        {
            // Factories für das Erzeugen von Modellelementen (Std und Hybrid)
            SF = (ShapeFactory)myPart.ShapeFactory;
            HSF = (HybridShapeFactory)myPart.HybridShapeFactory;

            // geometrisches Set auswaehlen und umbenennen
            HybridBodies catHybridBodies1 = myPart.HybridBodies;
            HybridBody catHybridBody1;

            try
            {
                catHybridBody1 = catHybridBodies1.Item("Geometrisches Set.1");
            }
            catch (Exception)
            {
                Console.WriteLine("Kein geometrisches Set gefunden! " + Environment.NewLine +
                    "Ein PART manuell erzeugen und ein darauf achten, dass 'Geometisches Set' aktiviert ist.",
                    "Fehler");
               
                return;
            }
            catHybridBody1.set_Name("Profile");

            // neue Skizze im ausgewaehlten geometrischen Set auf eine Offset Ebene legen
            mySketches = catHybridBody1.HybridSketches;
            OriginElements catOriginElements = myPart.OriginElements;
            Reference catReferenz = (Reference)catOriginElements.PlaneZX;
           hsp_catiaSkizze = mySketches.Add(catReferenz);




            // Part aktualisieren
            myPart.Update();
        }


    


        public void erzeugeNieten2(nietModel niete, double x, double z, String name)
        {
            
            
            OriginElements catOriginElements = myPart.OriginElements;
            Reference RefmyPlaneZX = (Reference)catOriginElements.PlaneZX;

           

            Sketch niet = nieten(name, x, z, RefmyPlaneZX);

            Bodies bodies = myPart.Bodies;
            myBody = myPart.MainBody;

            // Hauptkoerper in Bearbeitung definieren
            myPart.InWorkObject = myPart.MainBody;

            // Skizze umbenennen
            niet.set_Name(name);

            // Skizze...
            // ... oeffnen für die Bearbeitung
            Factory2D catFactory2D1 = niet.OpenEdition();

            // ... Kreis erstellen
    
            Point2D Ursprung = catFactory2D1.CreatePoint(x, z);
            Circle2D Kreis = catFactory2D1.CreateCircle(x, z, niete.durchmesser, 0, 0);
            Kreis.CenterPoint = Ursprung;
            

            Reference RefMySchaft = myPart.CreateReferenceFromObject(niet);
            mySchaft = SF.AddNewPadFromRef(RefMySchaft, niete.Laenge);

            // ... schliessen
            niet.CloseEdition();
            myPart.Update();
        }

        private Sketch nieten(String name, double x, double z, Reference RefmyPlaneZX) {
 

            Sketch myNiet = mySketches.Add(RefmyPlaneZX);

            myPart.InWorkObject = myNiet;
            myNiet.set_Name(name);
        
            object[] arr = new object[] {x, 0.0, 0.0,
                                         z, 0.0, 0.0,
                                         0.0, 0.0, 0.0 };

            myNiet.SetAbsoluteAxisData(arr);
            

            return myNiet;
        }


        public void einstellungen()
        {

            try
            {
                // Konsolen-Farbe ändern
                ConsoleColor originalColor = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.DarkGreen;

                Console.WriteLine("=================================");
                Console.WriteLine("Einstellungen");
                Console.WriteLine("\n");
                SettingControllers mySettings = hsp_experienceApp.SettingControllers;

                // Das passende SettingRepository erhalten. Entspricht dem Menüabschnitt "Einstellungen\Infrastruktur\3D Shape Infrastructure"
                SettingRepository shapeSettingRepository = (SettingRepository)mySettings.Item("3DShapeInfrastructure3DShape");


                // Sicherstellen, dass automatisch ein Achsensystem erzeugt wird (beim Erzeugen einer 3D-Form).
                bool NewWithAxisSystemEnabled = (bool)shapeSettingRepository.GetAttr("NewWithAxisSystem");
                if (NewWithAxisSystemEnabled == false)
                {
                    shapeSettingRepository.PutAttr("NewWithAxisSystem", true);
                    Console.WriteLine("Einstellung wurde auf \"True\" geändert.\n");
                }
                else
                {
                    Console.WriteLine("Die Einstellungen für das Achsensystem sind Aktuell");
                }

                // Sicherstellen, dass automatisch ein geometrisches Set erzeugt wird (beim Erzeugen einer 3D-Form).
                bool NewWithGSEnabled = (bool)shapeSettingRepository.GetAttr("NewWithGS");
                if (NewWithGSEnabled == false)
                {
                    shapeSettingRepository.PutAttr("NewWithGS", true);
                    Console.WriteLine("Einstellung wurde auf \"True\" geändert.\n");
                }
                else
                {
                    Console.WriteLine("Die Einstellungen für das Geometrische Set sind Aktuell");
                }

                // Hybridkonstruktion ermöglichen.
                bool HybridDesignModeEnabled = (bool)shapeSettingRepository.GetAttr("HybridDesignMode");
                if (HybridDesignModeEnabled == false)
                {
                    shapeSettingRepository.PutAttr("HybridDesignMode", true);
                    Console.WriteLine("Einstellung wurde auf \"True\" geändert.\n");
                }
                else
                {
                    Console.WriteLine("Die einstellungen für Hybridkonstruktionen sind Aktuell");
                }

                // Speichern der Einstellungen.

                shapeSettingRepository.SaveRepository();

                Console.WriteLine("=================================");

                // Konsolen-Farbe zurücksetzen auf ursprünglichen Wert
                Console.ForegroundColor = originalColor;

            }
            catch (Exception)
            {
                throw new Exception("Beim Bearbeiten der Einstellungen ist ein Fehler aufgetreten.");
            }
        }



        public void dxImport() {
            // Konsolen-Farbe ändern
            ConsoleColor originalColor = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.DarkRed;

            Console.WriteLine("=================================");
            Console.WriteLine("Import");
            Console.WriteLine("\n");

            string filePath = Path.Combine(Directory.GetCurrentDirectory(), @"Models"+ "\\BG_Verbund_A.1.3dxml");
            Console.WriteLine(filePath);

            Console.WriteLine("=================================");
            // Konsolen-Farbe zurücksetzen auf ursprünglichen Wert
            Console.ForegroundColor = originalColor;
        }


        public void CreateAxisSystem(int x, int y, int z)
        {

            // Neues Achsensystem erzeugen.
            AxisSystem newAxisSystem = myPart.AxisSystems.Add();
            newAxisSystem.set_Name("Neues Achsensystem");

            // Typen festlegen.
            newAxisSystem.Type = CATAxisSystemMainType.catAxisSystemStandard;
            newAxisSystem.OriginType = CATAxisSystemOriginType.catAxisSystemOriginByCoordinates;
            newAxisSystem.XAxisType = CATAxisSystemAxisType.catAxisSystemAxisByCoordinates;
            newAxisSystem.YAxisType = CATAxisSystemAxisType.catAxisSystemAxisByCoordinates;
            newAxisSystem.ZAxisType = CATAxisSystemAxisType.catAxisSystemAxisByCoordinates;
            


            object[] originArray = new object[] { x, y, z };
            object[] xAxisArray = new object[] { 1, 20, 0 };
            //object[] yAxisArray = new object[] { 1, 20, 0 };
            object[] zAxisArray = new object[] { 0,20, 0 };
            newAxisSystem.PutOrigin(originArray);
            //newAxisSystem.PutXAxis(xAxisArray);
            //newAxisSystem.PutYAxis(yAxisArray);
            //newAxisSystem.PutZAxis(zAxisArray);
            
            myPart.Update();
        }

        public void nietePlatte(nietModel niete, double x, double z, String name) {

            Editor activeEditor = hsp_experienceApp.ActiveEditor;
            myPart = (Part)activeEditor.ActiveObject;
            ShapeFactory shapefactory = (ShapeFactory)myPart.ShapeFactory;

            // Hauptkörper in Bearbeitung setzen.
            myPart.InWorkObject = myPart.MainBody;

            OriginElements catOriginElements = myPart.OriginElements;
            Reference RefmyPlaneZX = (Reference)catOriginElements.PlaneZX;

            // Factories für das Erzeugen von Modellelementen (Std und Hybrid)
            SF = (ShapeFactory)myPart.ShapeFactory;
            HSF = (HybridShapeFactory)myPart.HybridShapeFactory;

            // geometrisches Set auswaehlen und umbenennen
            HybridBodies catHybridBodies1 = myPart.HybridBodies;
            HybridBody catHybridBody1;
            catHybridBody1 = catHybridBodies1.Item("Geometrisches Set.1");
            mySketches = catHybridBody1.HybridSketches;

            Sketch niet = nieten(name, x, z, RefmyPlaneZX);

            Bodies bodies = myPart.Bodies;
            myBody = myPart.MainBody;

            // Hauptkoerper in Bearbeitung definieren
            myPart.InWorkObject = myPart.MainBody;

            // Skizze umbenennen
            niet.set_Name(name);

            // Skizze...
            // ... oeffnen für die Bearbeitung
            Factory2D catFactory2D1 = niet.OpenEdition();

            // ... Kreis erstellen

            Point2D Ursprung = catFactory2D1.CreatePoint(x, z);
            Circle2D Kreis = catFactory2D1.CreateCircle(x, z, niete.durchmesser, 0, 0);
            Kreis.CenterPoint = Ursprung;


            Reference RefMySchaft = myPart.CreateReferenceFromObject(niet);
            mySchaft = SF.AddNewPadFromRef(RefMySchaft, niete.Laenge);

            // ... schliessen
            niet.CloseEdition();
            myPart.Update();


        }


    }
}
