﻿using System;
using System.IO;

namespace niet_3dx
{
    public class niet3DxControll
    {
        public niet3DxControll()
        {
            try
            {

                niet3DxConnection ni = new niet3DxConnection();

                // Finde Catia Prozess
                if (ni.CATIALaeuft())
                {
                    nietModel niete = new nietModel(10, 2);

                    ni.nietePlatte(niete, 20, 80, "niete3");



                    /*
                    // 3Dx Einstellungen anpassen
                    ni.einstellungen();

                    //Importieren
                    ni.dxImport();

                    

                    // Konsolen-Farbe ändern
                    ConsoleColor originalColor = Console.ForegroundColor;
                    Console.ForegroundColor = ConsoleColor.DarkBlue;

                    Console.WriteLine("=================================");
                    Console.WriteLine("PartDesign");
                    Console.WriteLine("\n");
  

                    // Öffne ein neues Part
                    ni.Erzeuge3DShape();
                    Console.WriteLine("3D Shape erzeugt");

                    // Erstelle eine Skizze
                    ni.ErstelleLeereSkizze();
                    Console.WriteLine("Leere Skizzze erstellt");

                    // neue Axensysteme
                    ni.CreateAxisSystem(20, 0,20);
                    ni.CreateAxisSystem(30, 0, 30);
                    ni.CreateAxisSystem(40, 0, 40);
                    Console.WriteLine("Axensysteme Erstellt");

                    nietModel niete = new nietModel(10, 2);

                    ni.erzeugeNieten2(niete, 0, 0, "niete1");
                    Console.WriteLine("Niete 1 erstellt");

                    ni.erzeugeNieten2(niete,10,10,"niete2");
                    Console.WriteLine("Niete 2 erstellt");

                    ni.erzeugeNieten2(niete, 20, 80, "niete3");
                    Console.WriteLine("Niete 3 erstellt");
                    Console.WriteLine("=================================");

                    // Konsolen-Farbe zurücksetzen auf ursprünglichen Wert
                    Console.ForegroundColor = originalColor; 
                    */
                }
                else
                {
                    Console.WriteLine("Laufende 3DEXPERIENCE-Applikation nicht gefunden");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        static void Main(string[] args)
        {
            new niet3DxControll();
        }

    }


}

